const express = require('express');
const redis = require('redis');

// 컨테이너 restart policy 테스트
//const process = require('process');

const app = express();
const client = redis.createClient({
    host: 'redis-server',
    port: 6379 // default port number
});
client.set('visits', 0);

app.get('/', (req, res) => {
    // 컨테이너 restart policy 테스트
    // process.exit(0);

    client.get('visits', (err, visits) => {
        // display
        res.send('Number of visits is ' + visits);
        
        // update
        client.set('visits', parseInt(visits) + 1);
    });
});

app.listen(8081, ()=>{
    console.log('Listening on port 8081');
}) 