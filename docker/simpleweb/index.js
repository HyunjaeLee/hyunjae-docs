const express = require('express');

const app = express();

// route handler
/*  anytime someone visits the root route of our application,
    we are going to immediately send back a string that says 'Bye there' */
app.get('/', (req, res) => {
    res.send('How are you doing?');
});

// listen
app.listen(8080, () => {
    console.log('Listening on port 8080');
});